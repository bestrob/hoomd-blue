// Copyright (c) 2009-2016 The Regents of the University of Michigan
// This file is part of the HOOMD-blue project, released under the BSD 3-Clause Licence


// Maintainer: gld215

#ifndef __PAIR_EVALUATOR_KH_H__
#define __PAIR_EVALUATOR_KH_H__

#ifndef NVCC
#include <string>
#endif

#include "hoomd/HOOMDMath.h"

/*! \file EvaluatorPairKH.h
    \brief Defines the pair evaluator class for Kim-Hummer potential
*/

// need to declare these class methods with __device__ qualifiers when building in nvcc
#ifdef NVCC
#define DEVICE __device__
#define HOSTDEVICE __host__ __device__
#else
#define DEVICE
#define HOSTDEVICE
#endif

//! KH parameters
/*!
 * See EvaluatorPairKH for details of the parameters.
 */
//struct kh_params
//    {
//    Scalar lj1; //<! The coefficient for 1/r^12
//    Scalar lj2; //!< The coefficient for 1/r^6
//    Scalar eps2; //!< Epsilon parameter multiplied by 2
//    Scalar rrepsq; //!< Square of distance to switch functional forms for repulsive potential
//    };
//
////! Convenience function for making a kh_params in python
//HOSTDEVICE inline kh_params make_kh_params(Scalar lj1, Scalar lj2, Scalar eps2, Scalar rrepsq)
//    {
//    kh_params p;
//    p.lj1 = lj1;
//    p.lj2 = lj2;
//    p.eps2 = eps2;
//    p.rrepsq = rrepsq;
//    return p;
//    }
//
//! Class for computing Kim-Hummer potential and forces
class EvaluatorPairKH
    {
    public:
        //! Define the parameter type used by this pair potential evaluator
        typedef Scalar4 param_type;

        //! Constructs the pair potential evaluator
        /*! \param _rsq Squared distance beteen the particles
            \param _rcutsq Sqauared distance at which the potential goes to 0
            \param _params Per type pair parameters of this potential
        */
        DEVICE EvaluatorPairKH(Scalar _rsq, Scalar _rcutsq, const param_type& _params)
            : rsq(_rsq), rcutsq(_rcutsq), lj1(_params.x), lj2(_params.y), eps2(_params.z),
              rrepsq(_params.w)
            {
            }

        //! KH doesn't use diameter
        DEVICE static bool needsDiameter() { return false; }
        //! Accept the optional diameter values
        /*! \param di Diameter of particle i
            \param dj Diameter of particle j
        */
        DEVICE void setDiameter(Scalar di, Scalar dj) { }

        //! KH doesn't use charge
        DEVICE static bool needsCharge() { return false; }
        //! Accept the optional diameter values
        /*! \param qi Charge of particle i
            \param qj Charge of particle j
        */
        DEVICE void setCharge(Scalar qi, Scalar qj) { }

        //! Evaluate the force and energy
        /*! \param force_divr Output parameter to write the computed force divided by r.
            \param pair_eng Output parameter to write the computed pair energy
            \param energy_shift If true, the potential must be shifted so that V(r) is continuous at the cutoff

            \return True if they are evaluated or false if they are not because we are beyond the cuttoff
        */
        DEVICE bool evalForceAndEnergy(Scalar& force_divr, Scalar& pair_eng, bool energy_shift)
            {
            if (rsq < rcutsq && lj1 != 0)
                {
                Scalar r2inv = Scalar(1.0)/rsq;
                Scalar r6inv = r2inv * r2inv * r2inv;
                force_divr= r2inv * r6inv * (Scalar(12.0)*lj1*r6inv - Scalar(6.0)*lj2);

                pair_eng = r6inv * (lj1*r6inv - lj2);

                if (eps2 >= 0 && rsq < rrepsq)
                    {
                    pair_eng += eps2;
                    }

                else
                    {
                    pair_eng *= -1;
                    force_divr *= -1;
                    }

                return true;
                }
            else
                return false;
            }

        #ifndef NVCC
        //! Get the name of this potential
        /*! \returns The potential name. Must be short and all lowercase, as this is the name energies will be logged as
            via analyze.log.
        */
        static std::string getName()
            {
            return std::string("kh");
            }
        #endif

    protected:
        Scalar rsq;     //!< Stored rsq from the constructor
        Scalar rcutsq;  //!< Stored rcutsq from the constructor
        Scalar lj1;     //!< lj1 parameter extracted from the params passed to the constructor
        Scalar lj2;     //!< lj2 parameter extracted from the params passed to the constructor
        Scalar eps2;    //!< epsilon parameter multiplied by 2
        Scalar rrepsq;  //!< cutoff radius squared for switching to repulsive potential
    };

#undef DEVICE
#undef HOSTDEVICE

#endif // __PAIR_EVALUATOR_KH_H__
